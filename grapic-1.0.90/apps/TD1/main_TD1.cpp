#include <Grapic.h>
using namespace grapic;

const int DIMW = 1000;

struct Complex
{
    float x;
    float y;
};

Complex make_complex(float x, float y)
{
    Complex z;
    z.x = x;
    z.y = y;
    return z;
}

Complex make_complex_exp(float r, float theta_deg)
{
    Complex z;
    z.x = r*cos(theta_deg*M_PI/180);
    z.y = r*sin(theta_deg*M_PI/180);
    return z;
}

Complex operator+(Complex a, Complex b)
{
    Complex c;
    c.x = a.x+b.x;
    c.y = a.y+b.y;
    return c;
}

Complex operator-(Complex a, Complex b)
{
    Complex c;
    c.x = a.x-b.x;
    c.y = a.y-b.y;
    return c;
}

Complex operator*(Complex a, Complex b)
{
    Complex c;
    c.x = a.x*b.x - a.y*b.y;
    c.y = a.x*b.y+a.y*b.x;
    return c;
}

Complex operator*(float lambda, Complex b)
{
    Complex c;
    c.x = lambda*b.x;
    c.y = lambda*b.y;
    return c;
}

Complex scale(Complex p, float cx, float cy, float lambda)
{
    return (lambda*(p-make_complex(cx,cy)) + make_complex(cx,cy));
}

Complex Rotate(Complex p, float cx, float cy, float theta_deg)
{
    Complex Ref = make_complex(cx,cy);
    Complex angle = make_complex_exp(1, theta_deg);
    return ((p-Ref)*angle + Ref);
}

struct SolarSystem
{
    Complex Soleil;
    Complex Mercure;
    Complex Terre;
    Complex Lune;
};

void init(SolarSystem& s)
{
    s.Soleil = make_complex(DIMW/2, DIMW/2);
    s.Mercure = s.Soleil + make_complex(100,0);
    s.Terre = s.Soleil + make_complex(250,0);
    s.Lune = s.Terre + make_complex(20,0);
}

void draw(SolarSystem s)
{
    color(255,255,0);
    circleFill(s.Soleil.x, s.Soleil.y, 50);
    color(0,255,0);
    circleFill(s.Mercure.x, s.Mercure.y, 20);
    color(0,0,255);
    circleFill(s.Terre.x, s.Terre.y, 15);
    color(135,135,135);
    circleFill(s.Lune.x, s.Lune.y, 5);
}

void update(SolarSystem& s)
{
    s.Mercure = Rotate(s.Mercure, s.Soleil.x, s.Soleil.y, 0.01);
    s.Terre = Rotate(s.Terre, s.Soleil.x, s.Soleil.y, 0.003);
    s.Lune = Rotate(s.Lune, s.Soleil.x, s.Soleil.y, 0.003);
    s.Lune = Rotate(s.Lune, s.Terre.x, s.Terre.y, 0.02);
}

int main(int , char** )
{
    bool stop=false;
	winInit("MyProg", DIMW, DIMW);
    backgroundColor(0, 0, 0);
    SolarSystem s;
    init(s);
	while( !stop )
    {
        winClear();
        draw(s);
        update(s);
        stop = winDisplay();
    }
    winQuit();
	return 0;
}

