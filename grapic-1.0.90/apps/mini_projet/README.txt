J'ai cr�e ce mini-projet nomm� Spacebattle qui est un jeu 2D dans l'espace o� l'on contr�le un vaisseau spatial qui doit d�truire les m�t�orites qui tombent sur la Terre.
Nous disposons de 3 vies.

Cette semaine j'ai r�cup�r� les images/sprites pour le vaisseau, les ast�ro�des et le fond. je les ai mis en place tout en randomisant l'apparition des ast�ro�des sur 
la moiti� haute de l'�cran.

La semaine prochaine, je veux au moins avoir mis en place un visuel des vies et du score, et le d�placement des ast�roides.

Je me suis beaucoup inspir� du TP d'ast�ro�des du cours mais j'esp�re que mes modifications seront assez cons�quentes.


SEMAINE 2 & 3:

J'ai mis en place diff�rent coloris d'ast�roide en fonction de sa duret�, ajout� la vitesse de chute des m�t�orite qui diff�rent entre elles, changer le sprite du vaisseau,
ajout� le tir, d�placement du vaisseau, collision entre le vaisseau et les tirs, rajouter le score, les vies , le game over, le lancement du jeu, explosion des ast�roides,
etc...

pour la semaine prochaine qui est la derni�re, je compte rajouter 2 niveaux : un 2e avec les m�t�orites plus rapides et un 3e avec les m�t�orites encore plus rapides + un 
boss final si j'ai le temps.
Il faut de plus que j'ajoute un timer des explosions des m�t�orites que je n'arrive pas a faire.
+ explosion du vaisseau lorsqu'on perd.


SEMAINE 4 : 

ajout de plusieurs niveaux : 3 niveaux d'ast�roides + 1 boss final (r�duction tailles ast�roides + augmentation vitesse de chute des ast�roides ) , ajout d'un boss final au
niveau 4 qui est un vaisseau ennemi tirant 4 lasers en m�me temps
ajout des explosions des ast�roides, (timer d'une secondes) + commentaires sur les lignes/fonction 


MANUEL D'UTILISATION :

fl�ches directionnelles droite & gauche pour se d�placer � gauche et � droite.
ESPACE pour tirer.
"P" pour mettre pause/enlever la pause
appuyer sur "r" ou cliquer sur init en bas a gauche pour recommencer le jeu.


Vous �tes un vaisseau spatial prot�geant la plan�te de chutes d'ast�roide et d'un vaisseau ennemi. Pour cela, il faut d�truire les ast�roides tombant sur la Terre et 
le vaisseau ennemi.

Vous disposez de 3 vies indiqu�es en haut a droite par des coeurs. (laissez tomber un ast�roide ou entrer en collision avec un ast�roide fait perdre une vie, se faire 
toucher par les tirs ennemis aussi.)