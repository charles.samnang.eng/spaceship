#include <Grapic.h>
#include <stdlib.h>

#define Vec2 Complex //consid�re toutes les struct Vec2 en Complex
#define Point2d Complex //pareil qu'en haut pour Point2d
#define MAX_ASTEROID 10
#define MAX_TIR 3
#define TIR_BOSS 9

using namespace grapic;

const int DIMW = 1000;

struct Complex
{
    float x;
    float y;
};

Complex make_complex(float x, float y)
{
    Complex z;
    z.x = x;
    z.y = y;
    return z;
}

Complex make_complex_exp(float r, float theta_deg)
{
    Complex z;
    z.x = r*cos(theta_deg*M_PI/180);
    z.y = r*sin(theta_deg*M_PI/180);
    return z;
}

Complex operator+(Complex a, Complex b)
{
    Complex c;
    c.x = a.x+b.x;
    c.y = a.y+b.y;
    return c;
}

Complex operator-(Complex a, Complex b)
{
    Complex c;
    c.x = a.x-b.x;
    c.y = a.y-b.y;
    return c;
}

Complex operator*(Complex a, Complex b)
{
    Complex c;
    c.x = a.x*b.x - a.y*b.y;
    c.y = a.x*b.y+a.y*b.x;
    return c;
}

Complex operator*(float lambda, Complex b)
{
    Complex c;
    c.x = lambda*b.x;
    c.y = lambda*b.y;
    return c;
}

Complex operator*(Complex b, float lambda)
{
    Complex c;
    c.x = lambda*b.x;
    c.y = lambda*b.y;
    return c;
}

Complex operator/(Complex a, float lambda)
{
    Complex c;
    c.x = a.x/lambda;
    c.y = a.y/lambda;
    return c;
}

Complex scale(Complex p, float cx, float cy, float lambda)
{
    return (lambda*(p-make_complex(cx,cy)) + make_complex(cx,cy));
}

Complex Rotate(Complex p, float cx, float cy, float theta_deg)
{
    Complex Ref = make_complex(cx,cy);
    Complex angle = make_complex_exp(1, theta_deg);
    return ((p-Ref)*angle + Ref);
}

Complex translate(Complex p, float cx, float cy)
{
    Complex res;
    res.x = p.x + cx;
    res.y = p.y + cy;
    return res;
}

Complex Complex_Interp(Complex a, Complex b, float t)
{
    Complex ab = b-a;
    Complex c = a +t *ab;
    return c;
}

struct Particle
{
    float masse;
    Vec2 forces;
    Vec2 vitesse;
    Point2d position;
};

Particle partInit(Particle& parti, Point2d p, Vec2 vitesse, float m)
{
    parti.masse = m;
    parti.vitesse = vitesse;
    parti.position = p;
    parti.forces = make_complex(0,0);
}

void partAddForce(Particle& p, Vec2 force)
{
    p.forces = p.forces + force;
}

void partUpdatePV(Particle& p, float temps)
{
    if(p.masse >=0)
    {
        p.vitesse = p.vitesse + p.forces/(p.masse * temps);
        p.position = p.position + p.vitesse*temps;
        p.forces = make_complex(0,0);
    }
}

struct Asteroid
{
    Vec2 pos;           ///position de l'ast�roide
    Vec2 vitesse;       ///vitesse de l'ast�roide
    int radius;         ///rayon de l'ast�roide
    int life;           ///duret� de l'ast�roide
    float tempsMort;    ///temps de la mort de l'ast�roide
};

struct laser
{
    Vec2 pos;           ///position du tir
    Vec2 vitesse;       ///vitesse du tir
    int largeur;        ///largeur du tir
    int longueur;       ///longueur du tir
    int life;           ///vie du tir
    bool tirer;         ///bool�en pour savoir si le laser est tir� ou non.
};

struct Boss
{
    Vec2 pos;
    int largeur;
    int longueur;
    int vie;
    bool tirer;
    int nbTirBoss;
    laser tirBoss[TIR_BOSS];
};

struct World
{
    Asteroid ast[MAX_ASTEROID];         /// tableau d'ast�roides contenant les informations de chaque ast�roides
    int nb_ast;                 ///entier contenant le nombre d'ast�roides encore en vie dans ce niveau
    int score;                  ///entier contenant le score du joueur
    int vie;                  ///entier contenant le nb de vie du joueur
    int nb_tir;             ///entier contenant le nombre de tir actuel sur l'�cran.
    int niveau;             ///niveau actuel
    bool pause;             /// bool�en permettant de savoir si le jeu est en pause ou non. 1 = jeu en pause, 0 = jeu en marche
    float tempsNiveau;         /// r�el contenant le temps de lancement de chaque niveau afin de mettre une pause entre chaque niveau
    Boss boss;              ///structure du boss final au niveau 4
    Particle vaisseau;
    laser tir[MAX_TIR];
    Image spaceship;
    Image asteroide;
    Image asteroide2;
    Image asteroide3;
    Image explosion1;
    Image explosion2;
    Image explosion3;
    Image planete;
    Image heart;
    Image beam;
    Image ennemi;
    Image beamBoss;
};

void initAsteroide(World& w, int& niveau)
{
    int i;
    for(i=0; i<MAX_ASTEROID; i++)
    {
        w.ast[i].radius = 50 + (rand()%250)/niveau; ///randomise la taille des ast�roides
        w.ast[i].pos.x = w.ast[i].radius/2 + rand()%(DIMW-w.ast[i].radius) ;  ///positionne les ast�roides al�atoirement sur l'�cran sur l'axe horizontal - rayon de l'ast�roide pour pas qu'il sorte de l'�cran
        w.ast[i].pos.y = DIMW +  rand()%(2*DIMW); ///randomise la position des ast�roides sur la partie haute de l'�cran
        w.ast[i].vitesse = make_complex(0, niveau+rand()%5); ///randomise la vitesse des ast�roides entre 5 niveaux
        w.ast[i].life = 1+rand()%3; ///randomise la duret� des ast�roides
        w.ast[i].tempsMort = -1; ///initialise le temps de la mort de l'ast�roide a -1
    }
}

void initTir(World& w, int num_tir)
{
        w.tir[num_tir].pos = make_complex(w.vaisseau.position.x-4, w.vaisseau.position.y+95);
        w.tir[num_tir].vitesse.y = 5;
        w.tir[num_tir].life = 1;
        w.tir[num_tir].tirer = 0;
        w.tir[num_tir].largeur = 10;
        w.tir[num_tir].longueur = 50;
}

void initLaserBoss(World& w)
{
    int i;
    for (i=0; i<= TIR_BOSS; i++)
    {
        w.boss.tirBoss[i].largeur = 15;
        w.boss.tirBoss[i].longueur = 80;
        w.boss.tirBoss[i].life = 1;
        w.boss.tirBoss[i].vitesse = make_complex(0,4);
        w.boss.tirBoss[i].tirer = 0;
    }
    w.boss.tirBoss[0].pos = make_complex(w.boss.pos.x-152,w.boss.pos.y-160);
    w.boss.tirBoss[1].pos = make_complex(w.boss.pos.x-102,w.boss.pos.y-180);
    w.boss.tirBoss[2].pos = make_complex(w.boss.pos.x-75,w.boss.pos.y-200);
    w.boss.tirBoss[3].pos = make_complex(w.boss.pos.x-45,w.boss.pos.y-210);
    w.boss.tirBoss[4].pos = make_complex(w.boss.pos.x-10,w.boss.pos.y-240);
    w.boss.tirBoss[5].pos = make_complex(w.boss.pos.x+25,w.boss.pos.y-210);
    w.boss.tirBoss[6].pos = make_complex(w.boss.pos.x+60,w.boss.pos.y-200);
    w.boss.tirBoss[7].pos = make_complex(w.boss.pos.x+90,w.boss.pos.y-180);
    w.boss.tirBoss[8].pos = make_complex(w.boss.pos.x+140,w.boss.pos.y-160);
}

void initBoss(World& w)
{
    w.boss.nbTirBoss = 0;
    w.boss.vie = 50;
    w.boss.largeur = DIMW/3;
    w.boss.longueur = DIMW/3;
    w.boss.pos = make_complex(DIMW/2, DIMW);
    w.boss.tirer = 0 ;
}

void initWorld(World &w, int& niveau)
{
    int i;
    w.vie = 10;
    w.nb_ast = 10;
    w.score = 0;
    w.nb_tir = 0;
    w.boss.vie = 0;
    w.pause = 0 ;
    w.tempsNiveau = elapsedTime();
    w.niveau = niveau;
    w.spaceship = image("data/vaisseau.png");
    w.asteroide = image("data/asteroide.png");
    w.asteroide2 = image("data/asteroide2.png");
    w.asteroide3 = image("data/asteroide3.png");
    w.planete = image("data/planete.jpg");
    w.heart = image("data/heart.png");
    w.beam = image("data/beam.png");
    w.explosion1 = image("data/explosion1.png");
    w.explosion2 = image("data/explosion2.png");
    w.explosion3 = image("data/explosion3.png");
    w.ennemi = image("data/boss.png");
    w.beamBoss = image("data/beamBoss.png");
    initBoss(w);
    initLaserBoss(w);
    w.boss.vie =0;
    if(w.niveau < 4) ///3 niveaux d'ast�roides avant le boss
    {
        initAsteroide(w,w.niveau);
    }
    else ///niveau 4 = boss final donc initialisation
    {
        initBoss(w);
        initLaserBoss(w);
    }
    partInit(w.vaisseau, make_complex(DIMW/2, 0), make_complex(0, 0), 0); //initialise le vaisseau spatial au milieu en bas de l'�cran
    for(i=0; i< MAX_TIR; i++)
    {
        initTir(w,i);
    }
}

int reinit_tir(World& w,int i) ///r�initialise le tir n�i du boss.
{
    switch (i)
    {
    case 0:
        w.boss.tirBoss[0].pos = make_complex(w.boss.pos.x-152,w.boss.pos.y-160);
        w.boss.tirBoss[0].life = 1;
        w.boss.tirBoss[0].tirer = 0;
        break;
    case 1:
        w.boss.tirBoss[1].pos = make_complex(w.boss.pos.x-102,w.boss.pos.y-180);
        w.boss.tirBoss[1].life = 1;
        w.boss.tirBoss[1].tirer = 0;
        break;
    case 2:
        w.boss.tirBoss[2].pos = make_complex(w.boss.pos.x-75,w.boss.pos.y-200);
        w.boss.tirBoss[2].life = 1;
        w.boss.tirBoss[2].tirer = 0;
        break;
    case 3:
        w.boss.tirBoss[3].pos = make_complex(w.boss.pos.x-45,w.boss.pos.y-210);
        w.boss.tirBoss[3].life = 1;
        w.boss.tirBoss[3].tirer = 0;
        break;
    case 4:
        w.boss.tirBoss[4].pos = make_complex(w.boss.pos.x-10,w.boss.pos.y-240);
        w.boss.tirBoss[4].life = 1;
        w.boss.tirBoss[4].tirer = 0;
        break;
    case 5:
        w.boss.tirBoss[5].pos = make_complex(w.boss.pos.x+25,w.boss.pos.y-210);
        w.boss.tirBoss[5].life = 1;
        w.boss.tirBoss[5].tirer = 0;
        break;
    case 6:
        w.boss.tirBoss[6].pos = make_complex(w.boss.pos.x+60,w.boss.pos.y-200);
        w.boss.tirBoss[6].life = 1;
        w.boss.tirBoss[6].tirer = 0;
        break;
    case 7:
        w.boss.tirBoss[7].pos = make_complex(w.boss.pos.x+90,w.boss.pos.y-180);
        w.boss.tirBoss[7].life = 1;
        w.boss.tirBoss[7].tirer = 0;
        break;
    case 8:
        w.boss.tirBoss[8].pos = make_complex(w.boss.pos.x+140,w.boss.pos.y-160);
        w.boss.tirBoss[8].life = 1;
        w.boss.tirBoss[8].tirer = 0;
        break;
    default:
        break;
    }
}


void draw(World w)
{
    int i,j;
    int tempsActuel = 0;

    image_draw(w.planete, 0, 0,DIMW, DIMW); ///fond de l'�cran

    for(i=0; i < MAX_TIR; i++)
    {
        if(w.tir[i].tirer == 1)
        {
             image_draw(w.beam,w.tir[i].pos.x, w.tir[i].pos.y,w.tir[i].largeur,w.tir[i].longueur); ///dessine le faisceau laser
        }
    }
    if(w.vie > 0) ///dessine le vaisseau tant qu'il est en vie
    {
        image_draw(w.spaceship, w.vaisseau.position.x-50, w.vaisseau.position.y, 100, 100); /// dessine le vaisseau spatial
    }
    else
    {
        image_draw(w.explosion1, w.vaisseau.position.x-50, w.vaisseau.position.y, 100, 100); ///dessine une explosion lorsqu'on meurt
    }

    for(i=0; i<MAX_ASTEROID;i++) ///dessine une explosion de 1s lorsque les ast�roides meurent
    {
        if(w.ast[i].life == 0)
        {

            if(elapsedTime()-w.ast[i].tempsMort< 1)
            {
                if(w.ast[i].radius<100) ///images diff�rentes pour 3 tailles diff�rentes
            {
                image_draw(w.explosion1, w.ast[i].pos.x-w.ast[i].radius, w.ast[i].pos.y-w.ast[i].radius, 2*w.ast[i].radius, 2*w.ast[i].radius);
            }
                else if(w.ast[i].radius>=100 && w.ast[i].radius <200)
            {
                   image_draw(w.explosion2, w.ast[i].pos.x-w.ast[i].radius, w.ast[i].pos.y-w.ast[i].radius, 2*w.ast[i].radius, 2*w.ast[i].radius);
            }
                else if(w.ast[i].radius>=200)
            {
                   image_draw(w.explosion3, w.ast[i].pos.x-w.ast[i].radius, w.ast[i].pos.y-w.ast[i].radius, 2*w.ast[i].radius, 2*w.ast[i].radius);
            }
            }
        }
    }

    for(i=0; i<MAX_ASTEROID; i++) ///dessine les ast�roides en fonction de leur duret�
    {
        if(w.ast[i].life == 1)
        {
            image_draw(w.asteroide, w.ast[i].pos.x-w.ast[i].radius, w.ast[i].pos.y-w.ast[i].radius, 2*w.ast[i].radius, 2*w.ast[i].radius); //Dessine les ast�roides sur l'�cran
        }
        else if(w.ast[i].life == 2)
        {
            image_draw(w.asteroide2, w.ast[i].pos.x-w.ast[i].radius, w.ast[i].pos.y-w.ast[i].radius,2* w.ast[i].radius, 2*w.ast[i].radius);
        }
        else if(w.ast[i].life == 3)
        {
            image_draw(w.asteroide3, w.ast[i].pos.x-w.ast[i].radius, w.ast[i].pos.y-w.ast[i].radius, 2*w.ast[i].radius, 2*w.ast[i].radius);
        }
    }



    if(w.boss.vie > 0) ///dessine sur l'�cran le boss lorsqu'il est en vie.
    {
        image_draw(w.ennemi, w.boss.pos.x-w.boss.largeur/2, w.boss.pos.y-w.boss.longueur/2, w.boss.largeur, w.boss.longueur);
        for(j=0; j<= TIR_BOSS; j++)
        {
            if(w.boss.tirBoss[j].tirer == 1)
            {
                image_draw(w.beamBoss, w.boss.tirBoss[j].pos.x, w.boss.tirBoss[j].pos.y, w.boss.tirBoss[j].largeur, w.boss.tirBoss[j].longueur);
            }
        }
    }
    color(255,255,255);
    fontSize(30);
    print(10,DIMW-90,"Score :"); ///dessine "score : " en haut en gauche
    print(115,DIMW-90,w.score); ///imprime le score actuel
    if(w.niveau <4)
    {
        fontSize(50);
        print(10,DIMW-65,"Niveau");
        print(180,DIMW-65,w.niveau);
    }
    else
    {
        fontSize(50);
        print(10,DIMW-65,"BOSS FINAL");
    }

    if(w.pause == 1) ///�cris un message lorsque le jeu est en pause
    {
        fontSize(70);
        print(DIMW/2-70,DIMW/2, "PAUSE");
    }
    for(i=1; i<= w.vie; i++)
    {
        image_draw(w.heart, DIMW-50*i,DIMW-50,50,50); ///dessine le nb de vie en haut a droite
    }

}

void update(World& w)
{
    int i;
    int j;
    int aleatoire;
    int deplacer ;
    int tirEnnemi;
    int temps = elapsedTime();
    if(isKeyPressed(SDLK_p))        ///Appuyer sur "p" met soit le jeu en pause, soit le remet en marche
    {
        if(w.pause == 0)
        {
            w.pause = 1;
        }
        else
        {
            w.pause = 0;
        }
    }
    if(w.pause == 0)        ///actualise lorsque le jeu n'est pas en pause
    {
        for(i=0; i<MAX_ASTEROID; i++)
    {
        if(w.ast[i].life > 0) ///d�placement des ast�roides lorsqu'ils sont en vie
        {
          w.ast[i].pos = w.ast[i].pos - w.ast[i].vitesse/3;
        }
        if(w.ast[i].pos.y < (-w.ast[i].radius) && w.ast[i].life >= 1) ///le joueur perd une vie si il laisse passer un ast�roide
        {
            w.vie--;
            w.ast[i].life = 0;
            w.nb_ast--;
        }
        if(abs(w.ast[i].pos.x-w.vaisseau.position.x) < w.ast[i].radius+50 && (w.ast[i].pos.y - w.vaisseau.position.y) < w.ast[i].radius+50 && w.ast[i].life >=1) ///collision des asteroides avec le vaisseau
        {
            w.vie--;
            w.ast[i].life = 0;
            w.ast[i].tempsMort = elapsedTime();
            w.nb_ast--;
        }
        for(j=0; j<MAX_TIR; j++)
        {
            if(abs(w.ast[i].pos.x-w.tir[j].pos.x) < w.ast[i].radius && abs(w.ast[i].pos.y - w.tir[j].pos.y) < w.ast[i].radius && w.tir[j].tirer == 1 && w.ast[i].life > 0) ///collision entre les tirs et les ast�roides
            {
                w.ast[i].life--;
                w.tir[j].life --;
                w.nb_tir--;
                w.score++;
                if(w.ast[i].life == 0)
                {
                    w.nb_ast--;
                    w.ast[i].tempsMort = elapsedTime();
                }
            }
        }
    }
    for (j=0; j<MAX_TIR; j++)
    {
        if(abs(w.boss.pos.x-w.tir[j].pos.x) < w.boss.largeur/2 && abs(w.boss.pos.y - w.tir[j].pos.y) < w.boss.longueur/2 && w.tir[j].tirer == 1 && w.boss.vie > 0 && w.tir[j].life >0) ///collision entre les tirs et le boss
            {
                w.boss.vie--;
                w.tir[j].life --;
                w.nb_tir--;
                w.score++;
            }
    }

    if(isKeyPressed(SDLK_LEFT) && w.vaisseau.position.x > 50 && w.vie > 0) ///d�place le vaisseau a gauche lorsqu'on appuie sur la touche "fl�che gauche" du clavier
    {
        w.vaisseau.position.x = w.vaisseau.position.x - 50;
        for(i=0; i<MAX_TIR; i++)
        {
            if(w.tir[i].tirer == 0)
            {
                w.tir[i].pos.x = w.tir[i].pos.x - 50;
            }

        }
    }
    if(isKeyPressed(SDLK_RIGHT) && w.vaisseau.position.x < (DIMW-50) && w.vie > 0) ///d�place le vaisseau a droite lorsqu'on appuie sur la touche "fl�che droite" du clavier
    {
        w.vaisseau.position.x = w.vaisseau.position.x+50;
        for(i=0; i<MAX_TIR; i++)
        {
            if(w.tir[i].tirer == 0)
            {
                w.tir[i].pos.x = w.tir[i].pos.x + 50;
            }
        }
    }
    if(isKeyPressed(SDLK_SPACE) && w.nb_tir < 3 && w.vie > 0) ///limite le nombre de tir � 3 et tire lorsqu'on appuie sur la touche ESPACE
    {
        if(w.tir[0].tirer == 0)
        {
            w.tir[0].tirer = 1;
        }
        else if(w.tir[1].tirer == 0)
        {
            w.tir[1].tirer = 1;
        }
        else if(w.tir[2].tirer == 0)
        {
            w.tir[2].tirer = 1;
        }
        w.nb_tir++;
    }



    for(i=0; i<MAX_TIR; i++)
    {
        if(w.tir[i].tirer == 1)
        {
            w.tir[i].pos.y = w.tir[i].pos.y + w.tir[i].vitesse.y; ///d�place le tir lorsque l'�tat "tirer" est a 1
        }
        if(w.tir[i].pos.y > DIMW)
        {
            w.tir[i].life = 0; ///tue le tir lorsqu'il sort de l'�cran
        }
        if(w.tir[i].life == 0) ///permet de r�initialiser le tir sorti et ainsi que le vaisseau puisse retirer
        {
            initTir(w,i);
            w.nb_tir--;
        }
    }

    if(w.boss.vie > 0 && w.boss.nbTirBoss == 0 ) ///le vaisseau tire simultan�ment 4 faisceaux entre 9 canons et attend que tous les faisceaux soient sortis de l'�cran pour retirer 4 faisceaux.
    {
            do
            {
                tirEnnemi = rand()%9;
            if(w.boss.tirBoss[tirEnnemi].tirer == 0)
            {
                w.boss.tirBoss[tirEnnemi].tirer = 1;
                w.boss.nbTirBoss++;
            }
            }while (w.boss.nbTirBoss<4);

    }

    for(i=0; i<TIR_BOSS; i++)
    {
        if(w.boss.tirBoss[i].tirer == 1)
        {
            w.boss.tirBoss[i].pos.y = w.boss.tirBoss[i].pos.y - w.boss.tirBoss[i].vitesse.y; ///fait bouger le tir lorsque son �tat "tirer" est 1.
        }
        if(w.boss.tirBoss[i].pos.y < (-w.boss.tirBoss[i].longueur))
        {
            w.boss.tirBoss[i].life = 0; ///tue le tir lorsqu'il sort de l'�cran
        }
        if(w.boss.tirBoss[i].life == 0) ///r�initialise le tir lorsqu'il "meurt"
        {
            reinit_tir(w,i);
            w.boss.nbTirBoss--;
        }
        if(abs(w.boss.tirBoss[i].pos.x-w.vaisseau.position.x) < w.boss.tirBoss[i].largeur/2+50 && (w.boss.tirBoss[i].pos.y - w.vaisseau.position.y) < w.boss.tirBoss[i].longueur/2+50 && w.boss.tirBoss[i].life >=1) ///collision des tirs ennemis avec le vaisseau
        {
            w.vie--;
            w.boss.tirBoss[i].life = 0;
        }
    }
    }
}

void updateBoss(World& w,int i)  ///d�place le boss al�atoirement : soit a gauche, soit a droite, soit il ne bouge pas
{

    int aleatoire;
    if(i == 1)
    {
        aleatoire = rand()%3;
        if(aleatoire == 0 && w.boss.pos.x > w.boss.largeur/2)
        {
            w.boss.pos.x = w.boss.pos.x -200;
            for(i=0; i<TIR_BOSS; i++)
        {
            if(w.boss.tirBoss[i].tirer == 0)
            {
                w.boss.tirBoss[i].pos.x = w.boss.tirBoss[i].pos.x - 200;
            }

        }
        }
        if(aleatoire == 1 && w.boss.pos.x < DIMW-w.boss.largeur/2)
        {
            w.boss.pos.x = w.boss.pos.x +200;
            for(i=0; i<TIR_BOSS; i++)
        {
            if(w.boss.tirBoss[i].tirer == 0)
            {
                w.boss.tirBoss[i].pos.x = w.boss.tirBoss[i].pos.x + 200;
            }

        }
        }
    }
}



int main(int , char** )
{
    bool stop=false;
    World w;
    int time = elapsedTime();
    int scoreActuel;
    int i;
    int deplacement;
    int vieActuel;
    int niveau = 1;
	winInit("SpaceShip", DIMW, DIMW);
    backgroundColor(0, 0, 0);
    initWorld(w, niveau);
    Menu menu;
    menu_add(menu, "Init");
    menu_add(menu, "Run");
	while( !stop )
    {
        time=elapsedTime();
        winClear();
        draw(w);
        if(elapsedTime()- w.tempsNiveau > 3 ) ///met une pause de 3s entre chaque level
        {
          update(w);
        }
        else if(elapsedTime()- w.tempsNiveau < 3 && w.niveau < 4) ///�crit le niveau actuel pendant la pause entre chaque level
        {
            fontSize(50);
            print(DIMW/2-120, DIMW/2, "NIVEAU ");
            print(DIMW/2+70,DIMW/2, w.niveau);
        }
        else if(elapsedTime()- w.tempsNiveau < 3 && w.niveau >= 4)
        {
            fontSize(50);
            print(DIMW/2-120, DIMW/2, "BOSS FINAL");
        }
        if(w.vie > 0 && w.nb_ast == 0) ///lorsque le joueur finit le niveau, on passe au niveau suivant en gardant son score et son nombre de vies
        {
            scoreActuel = w.score +10*niveau;
            vieActuel = w.vie;
            niveau++;
            initWorld(w,niveau);
            w.score = scoreActuel;
            w.vie = vieActuel;
        }
        if(time%2 == 0) ///le vaisseau ennemi se d�place al�atoirement toutes les 2s
        {
            if(niveau == 4)
            {
                updateBoss(w,i);
                i=0;
            }
        }
        else
        {
            i=1;
        }

        if(niveau == 4 && w.boss.vie == 0)
        {
            fontSize(50);
            print(DIMW/2-200,DIMW/2, "VOUS AVEZ GAGN�");
        }

        if(w.vie <= 0) ///si le joueur n'a plus de vie, partie finie.
        {
            w.nb_ast = 0;
            for(i=0; i<MAX_ASTEROID; i++)
            {
                w.ast[i].life=0;
            }
            fontSize(70);
            print(DIMW/2-190,DIMW/2, "GAME OVER");
            fontSize(20);
            print(DIMW/2-160,DIMW/2-20, "Appuyez sur R pour recommencer");
        }
        if(menu_select(menu) == 0 || isKeyPressed(SDLK_r))
        {
            niveau = 1;
            initWorld(w,niveau);
            menu_setSelect(menu, 1);
        }
        menu_draw(menu);
        stop = winDisplay();
    }
    winQuit();
	return 0;
}

