#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;




/// LISEZ TOUS LES COMMENTAIRES AVEC ATTENTION !!!!
/// Vous devez respecter scrupuleusement les sections de r�ponses aux questions. UNE REPONSE HORS CASE NE SERA PAS CORRIGEE !
/// Vous respecterez scrupuleusement les constantes donn�es.  UNE CONSTANTE NON RESPECTEE ANNULERA LA REPONSE A LA QUESTION !
///
///
///
/// L'objectif de ce code est de tabuler une fonction d�finie par un polyn�me sur l'intervale [0;1].
/// Tabuler une fonction signifie stocker les valeurs de la fonction dans un tableau.
/// Chaque case du tableau stocke une �valuation de la fonction.
/// Pour �valuer la fonction, il suffit alors de regarder dans le tableau plut�t que d'en faire son calcul.
/// Ceci peut �tre utile quand la fonction � calculer prend du temps car la fonction est complexe.
///
/// Nous allons �valuer l'erreur faite par la fonction tabul�e en comparaison avec la fonction analytique (le polyn�me pour ce sujet).
/// Nous comparerons �galement la d�riv�e analytique avec le calcul de d�riv�e discr�te que nous avons souvent calcul�e en LIFAMI .
///
/// Nous tabulerons toujours la fonction F(x) avec x entre 0 et 1 (inclus) donc dans l'intervalle [0;1]
/// Il faudra �tre capable de passer de l'indice i du tableau � la valeur de x en connaissant la taille du tableau n
///
/// Un exemple de fonction tabul�e : F(x) = 2*x^2 + 1 avec un tableau de taille n=5. (Le ^ signifie puissance).
///  n=5
/// |-----------------------------------------------------------------------
/// | i            |  0      | 1         |  2        | 3         | 4       |
/// |-----------------------------------------------------------------------
/// | val[i]=F(x)  |  F(0)=  | F(0.25)=  | F(0.50)=  | F(0.75)=  | F(1.0)= |
/// |              |  1.0    | 1.125     | 1.5       | 2.125     | 3       |
/// |-----------------------------------------------------------------------
///
/// Comment calculer F(x) ?
/// On trouve la case inf�rieure la plus proche en calculant la partie_enti�re de ( x*(n-1) ).
/// Rappel : avec x=0 on a la case 0, avec x=1 on la case n-1 (la derni�re)
/// Puis on interpole entre cette case et la case sup�rieure.
///
/// Par exemple pour F(0.60) :
/// On cherche la case en dessous : numero_case = (n-1)*0.60 = (5-1)*0.60 = 2.40.  La case est la num�ro 2.
/// On va interpoler entre la case 2 et la case 2+1=3
/// On caclule le param�tre t de l'interpolation en r�cup�rant la partie d�cimale : t = 2.40 - 2 = 0.40
/// Puis on calcule l'interpolation entre la case 2 et 3 avec le param�tre t.




/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 1 D�finissez une fonction F(x : reel)->reel retournant l'�valuation du polynome=6514*x^3 + 7933*x^2 + 3046*x + 1046
/// DEBUT REPONSE ID=7377

double F(double x)
{
    return (6514*pow(x,3)+7933*pow(x,2) + 3046*x+1046);
}

/// FIN REPONSE ID=7226



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 2 D�finissez une fonction F_deriv(x : reel)->reel retournant l'�valuation de la d�riv�e du polyn�me pr�c�dent
/// Vous calculerez ici la version analytique de la d�riv�e du polyn�me.
/// Par exemple la d�riv�e de 3*x^2 + 2*x + 2    est    6*x + 2
/// DEBUT REPONSE ID=301



double F_deriv(double x)
{
    return 3*6514*pow(x,2) + 7933*2*x+3046;
}

/// FIN REPONSE ID=4119



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 3 D�finissez une constante enti�re MAX_FONCTION=4692
/// DEBUT REPONSE ID=7838

const int MAX_FONCTION = 4692;

/// FIN REPONSE ID=7563



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 4 D�finissez une structure Fonction comportant les champs suivants.
///   val : un tableau de r�els de taille MAX_FONCTION
///   n : un entier indiquant le nombre de cases r�ellement utilis�es dans le tableau
/// Cette structure va contenir la version discr�te de la fonction d�finie plus haut
struct Fonction
{
/// DEBUT REPONSE ID=7533
double val[MAX_FONCTION];
int n;
/// FIN REPONSE ID=902
};



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 5 D�finissez une proc�dure initFonction(f : donn�e-r�sultat Fonction; n : donn�e entier)
/// Cette fonction va remplir les n cases du tableau val avec la fonction F d�finie plus haut.
/// Chaque case du tableau stocke une �valuation de la fonction analytique F(x).
/// Vous �valuerez la fonction avec x compris entre 0.0 et 1.0, donc dans l'intervalle [0.0 ; 1.0]
/// La case 0 du tableau stockera la valeur F(0),
/// La case i du tableau stockera la valeur F(x) avec x �tant entre 0.0 et 1.0 en fonction de i
/// La case f.n-1 du tableau stockera F(1.0)
/// DEBUT REPONSE ID=6836


void initFonction(Fonction& f, int n)
{
    int i;
    for(i=0; i<n; i++)
    {
        f.val[i] = F(i/(n-1));
    }
}






/// FIN REPONSE ID=3918



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 6 D�finissez une fonction evalFonction(f : donn�e Fonction; x : donn�e reel)->r�el
/// Cette fonction fera une interpolation entre les 2 cases du tableaux pour retrouver la valeur de la fonction
/// Rappel ; la case 0 correspond � x=0.0 et la derni�re case de num�ro f.n-1 correspond � x=1.0 (Faites une proportionnalit�)
/// Par exemple si f.n=100 : un tableau allant de la case 0 � la case 99
/// Si x=0.7 =>  numero_case = 0.7*99 = 69.3
/// La valeur de la fonction se trouve en faisant une interpolation
/// entre la case 69 (avec comme poids 1.0-0.3) et la case 70 (avec comme poids 0.3)
float evalFonction(Fonction f, float x)
{
    /// a. D�clarez un r�el 'numero_case' et lui affecter la case du tableau qui correspond � x avec  x * (la taille du tableau-1)
    /// b. D�clarez un entier 'numero_case_entier' et lui affecter la partie enti�re du r�el  'numero_case'
    ///    Rappel : si r est un r�el, int(r) donne la partie enti�re de r
    /// c. D�clarez un r�el t et calculer sa valeur
/// DEBUT REPONSE ID=9458
    float numero_case;
    numero_case = x * (f.n-1);
    int numero_case_entier;
    numero_case_entier = int(numero_case);
    float t;
    t = numero_case-numero_case_entier;

/// FIN REPONSE ID=1916

    /// d. Calculez la valeur de la fonction par interpolation
    ///    entre les cases numero_case_entier et numero_case_entier+1  (en utilisant le param�tre t)
    /// e. Puis retourner cette valeur en r�sultat de la fonction

/// DEBUT REPONSE ID=8309
    float resultat;
    resultat = numero_case_entier*t + (numero_case_entier+1)*(1-t);
    return resultat;
/// FIN REPONSE ID=4635
}



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 7 D�finissez une fonction derivFonction(f : donn�e Fonction; x : donn�e reel)->r�el
/// Cette fonction calcule la valeur de la d�riv�e de la fonction au point 'x'.
/// Vous utiliserez l'approximation de la d�riv�e que nous avons souvent utilis�e en LIFAMI.
/// D�finisez une variable locale de type r�el dans cette fonction : dx=0.007883481039041174
/// L'approximation de la d�riv�e utilis�e en LIFAMI est df(x)/dx = (  f(x)-f(x-dx) ) / dx
/// DEBUT REPONSE ID=3483


float derivFonction(Fonction f, float x)
{
    float dx=0.007883481039041174;
    float derivee;
    derivee = (evalFonction(f, x) - evalFonction(f, (x-dx)))/dx;
    return derivee;
}


/// FIN REPONSE ID=5044



float frand(float rmin, float rmax)
{
    float r = float(rand()) / RAND_MAX;
    return rmin + r * (rmax - rmin);
}


/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 8 D�finissez une fonction evalErreur(f : donn�e Fonction)->r�el
/// Cette fonction va �valuer l'erreur moyenne faite par la fonction tabul�e par rapport � la fonction exacte.
/// En tirant 473 valeurs au hasard entre 0.0 et 1.0, vous sommerez l'erreur faites sur l'�valuation
/// de la fonction et sur l'�valuation de la d�riv�e de la fonction. Puis vous moyennerez.
///
/// Rappel : F(x) donne la valeur exacte de la fonction. Et evalFonction(f,x) renvoie la valeur de la m�me fonction mais tabul�e.
/// Vous sommerez la valeur absolue des diff�rences entre ces deux fonctions.
/// Puis la fonction renverra l'erreur moyenne.
/// Vous pouvez utiliser la fonction frand d�finie juste au dessus.
/// DEBUT REPONSE ID=2969

float evalErreur(Fonction f)
{
    int i;
    float erreur;
    float sommeErreur=0;
    for(i=0; i<473; i++)
    {
        erreur = abs(evalFonction(f,frand(0.0,1.0))- F(frand(0.0,1.0)));
        sommeErreur = sommeErreur + erreur;
    }
    return (sommeErreur/473);
}
















/// FIN REPONSE ID=1673



/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 10-BONUS : pour ceux qui ont termin� avec l'algorithme principal (FAITES LA QUESTION 9 EN PREMIER, VOIR PLUS BAS)
/// Ecrivez une proc�dure drawFonction qui dessine la fonction analytique et la fonction tabul�e avec Grapic/plot_draw
/// Voir comment utiliser Grapic/plot ici : https:///perso.liris.cnrs.fr/alexandre.meyer/grapic/html/index.html#tuto8
/// DEBUT REPONSE ID=7314



/// FIN REPONSE ID=6183


/// -----------------------------------------------------------------------------------------------------------------
/// QUESTION 9 Ecrivez un algorithme principal qui teste les fonctions pr�c�dentes
/// Vous testerez plusieurs tailles de tableau pour la fonction tabul�e afin de comparer les valeurs d'erreur.
int main(int, char ** )
{
    int i;
    float x, Fx, fx, dx, ex;
    Fonction f;
    /// /// D�commentez les lignes suivantes pour tester votre code

    initFonction(f, 100);
    x = frand(0.f, 1.f);
    Fx = F(x);
    fx = evalFonction(f,x);
    cout<<"fonction analytique F("<<x<<")="<<Fx<<"   ";
    cout<<"fonction tabulee    F("<<x<<")="<<fx<<"   ";
    cout<<"erreur="<<fabs(Fx-fx)<<endl;


/// DEBUT REPONSE ID=1645

    dx = derivFonction(f,x);
    ex = evalErreur(f);

    cout<<"derivee fonction F("<<x<<") = "<<dx<<"  ";
    cout<<"erreur fonction ="<<ex<<endl;














/// FIN REPONSE ID=4155
    return 0;
}
