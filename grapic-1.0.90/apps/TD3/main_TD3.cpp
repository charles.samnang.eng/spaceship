#include <Grapic.h>

#define Vec2 Complex //consid�re toutes les struct Vec2 en Complex
#define Point2d Complex //pareil qu'en haut pour Point2d
#define MAX_ASTEROID 100

using namespace grapic;

const int DIMW = 1000;

struct Complex
{
    float x;
    float y;
};

Complex make_complex(float x, float y)
{
    Complex z;
    z.x = x;
    z.y = y;
    return z;
}

Complex make_complex_exp(float r, float theta_deg)
{
    Complex z;
    z.x = r*cos(theta_deg*M_PI/180);
    z.y = r*sin(theta_deg*M_PI/180);
    return z;
}

Complex operator+(Complex a, Complex b)
{
    Complex c;
    c.x = a.x+b.x;
    c.y = a.y+b.y;
    return c;
}

Complex operator-(Complex a, Complex b)
{
    Complex c;
    c.x = a.x-b.x;
    c.y = a.y-b.y;
    return c;
}

Complex operator*(Complex a, Complex b)
{
    Complex c;
    c.x = a.x*b.x - a.y*b.y;
    c.y = a.x*b.y+a.y*b.x;
    return c;
}

Complex operator*(float lambda, Complex b)
{
    Complex c;
    c.x = lambda*b.x;
    c.y = lambda*b.y;
    return c;
}

Complex operator*(Complex b, float lambda)
{
    Complex c;
    c.x = lambda*b.x;
    c.y = lambda*b.y;
    return c;
}

Complex operator/(Complex a, float lambda)
{
    Complex c;
    c.x = a.x/lambda;
    c.y = a.y/lambda;
    return c;
}

Complex scale(Complex p, float cx, float cy, float lambda)
{
    return (lambda*(p-make_complex(cx,cy)) + make_complex(cx,cy));
}

Complex Rotate(Complex p, float cx, float cy, float theta_deg)
{
    Complex Ref = make_complex(cx,cy);
    Complex angle = make_complex_exp(1, theta_deg);
    return ((p-Ref)*angle + Ref);
}

Complex translate(Complex p, float cx, float cy)
{
    Complex res;
    res.x = p.x + cx;
    res.y = p.y + cy;
    return res;
}

Complex Complex_Interp(Complex a, Complex b, float t)
{
    Complex ab = b-a;
    Complex c = a +t *ab;
    return c;
}

struct Particle
{
    float masse;
    Vec2 forces;
    Vec2 vitesse;
    Point2d position;
};

Particle partInit(Particle& parti, Point2d p, Vec2 vitesse, float m)
{
    parti.masse = m;
    parti.vitesse = vitesse;
    parti.position = p;
    parti.forces = make_complex(0,0);
}

void partAddForce(Particle& p, Vec2 force)
{
    p.forces = p.forces + force;
}

void partUpdatePV(Particle& p, float temps)
{
    if(p.masse >=0)
    {
        p.vitesse = p.vitesse + p.forces/(p.masse * temps);
        p.position = p.position + p.vitesse*temps;
        p.forces = make_complex(0,0);
    }
}

struct Asteroid
{
    Vec2 pos;
    float radius;
};

struct World
{
    Asteroid ast[MAX_ASTEROID];
    int nb_ast;
    int score;
    Particle vaisseau;
    Image spaceship;
    Image asteroide;
    Image planete;
};

void initWorld(World &w, int nb)
{
    int i;
    w.nb_ast = nb;
    partInit(w.vaisseau, make_complex(DIMW/2, 0), make_complex(0, 0), 0);
    w.spaceship = image("data/vaisseau.png", true, 255, 255, 255, 255);
    w.asteroide = image("data/asteroide.png", true, 255,255,255,255);
    w.planete = image("data/test.jpg", true, 255, 255, 255, 255);
    for(i=0; i<w.nb_ast; i++)
    {
        w.ast[i].pos.x = rand()%DIMW;
        w.ast[i].pos.y = 500 + rand()%(DIMW-500);
        w.ast[i].radius = 50 + rand()%100;
    }
}

void draw(World w)
{
    int i;
    image_draw(w.planete, 0, 0,DIMW, DIMW);
    image_draw(w.spaceship, w.vaisseau.position.x, w.vaisseau.position.y, 100, 100);
    for(i=0; i<w.nb_ast; i++)
    {
        image_draw(w.asteroide, w.ast[i].pos.x-w.ast[i].radius/2, w.ast[i].pos.y-w.ast[i].radius/2, w.ast[i].radius, w.ast[i].radius);
    }
}

int main(int , char** )
{
    bool stop=false;
    World w;
	winInit("SpaceShip", DIMW, DIMW);
    backgroundColor(0, 0, 0);
    initWorld(w, 10);
    Menu menu;
    menu_add(menu, "Init");
    menu_add(menu, "Run");
	while( !stop )
    {
        winClear();
        draw(w);

        if(menu_select(menu) == 0)
        {
            initWorld(w,10);
            menu_setSelect(menu, 1);
        }
        menu_draw(menu);
        stop = winDisplay();
    }
    winQuit();
	return 0;
}

