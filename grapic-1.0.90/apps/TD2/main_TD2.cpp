#include <Grapic.h>


#define MAXPOINTS 200

using namespace grapic;

const int DIMW = 1000;

struct Complex
{
    float x;
    float y;
};

Complex make_complex(float x, float y)
{
    Complex z;
    z.x = x;
    z.y = y;
    return z;
}

Complex make_complex_exp(float r, float theta_deg)
{
    Complex z;
    z.x = r*cos(theta_deg*M_PI/180);
    z.y = r*sin(theta_deg*M_PI/180);
    return z;
}

Complex operator+(Complex a, Complex b)
{
    Complex c;
    c.x = a.x+b.x;
    c.y = a.y+b.y;
    return c;
}

Complex operator-(Complex a, Complex b)
{
    Complex c;
    c.x = a.x-b.x;
    c.y = a.y-b.y;
    return c;
}

Complex operator*(Complex a, Complex b)
{
    Complex c;
    c.x = a.x*b.x - a.y*b.y;
    c.y = a.x*b.y+a.y*b.x;
    return c;
}

Complex operator*(float lambda, Complex b)
{
    Complex c;
    c.x = lambda*b.x;
    c.y = lambda*b.y;
    return c;
}

Complex scale(Complex p, float cx, float cy, float lambda)
{
    return (lambda*(p-make_complex(cx,cy)) + make_complex(cx,cy));
}

Complex Rotate(Complex p, float cx, float cy, float theta_deg)
{
    Complex Ref = make_complex(cx,cy);
    Complex angle = make_complex_exp(1, theta_deg);
    return ((p-Ref)*angle + Ref);
}

Complex translate(Complex p, float cx, float cy)
{
    Complex res;
    res.x = p.x + cx;
    res.y = p.y + cy;
    return res;
}

Complex Complex_Interp(Complex a, Complex b, float t)
{
    Complex ab = b-a;
    Complex c = a +t *ab;
    return c;
}

struct Polygon
{
    int nb;
    Complex p[MAXPOINTS];
};

#include "poly1.h"
#include "poly2.h"
#include "poly3.h"


void polygon_add(Polygon& poly, float px, float py)
{
    poly.p[poly.nb] = make_complex(px,py);
    poly.nb++;
}

void polygon_draw(Polygon poly)
{
    int i, j;
    for(i=0; i<poly.nb; i++)
    {
        j=(i+1)%poly.nb;
        line(poly.p[i].x, poly.p[i].y, poly.p[j].x, poly.p[j].y );
    }
}

Polygon Inter_2poly(Polygon poly1, Polygon poly2, float t)
{
    Polygon poly_res;
    int i;
    poly_res.nb = 0;
    for(i=0; i<poly1.nb; i++)
    {
        Complex p = Complex_Interp(poly1.p[i], poly2.p[i], t);
        polygon_add(poly_res, p.x, p.y);
    }
    return poly_res;
}

Complex centre_gravite(Polygon poly)
{
    Complex g = make_complex(0,0);
    int i;
    for(i=0; i<= poly.nb; i++)
    {
        g = g+poly.p[i];
    }
    g.x = g.x/poly.nb;
    g.y = g.y/poly.nb;
    return g;
}

void poly_translate(Polygon& p, float dx, float dy)
{
    Complex d = make_complex(dx, dy);
    int i;
    for(i=0; i<p.nb; i++)
    {
        p.p[i] = translate (p.p[i], dx, dy);
        p.p[i] = p.p[i] + d;
    }
}

void polygon_scale(Polygon& p, float cx, float cy, float lambda)
{
    int i;
    for(i=0; i<p.nb; i++)
    {
        p.p[i] = scale(p.p[i], cx, cy, lambda);
    }
}

void polygon_rotate(Polygon& p, float cx, float cy, float lambda)
{
    int i;
    for(i=0; i<p.nb; i++)
    {
        p.p[i] = Rotate(p.p[i], cx, cy, lambda);
    }
}

int main(int , char** )
{
    bool stop=false;
    Polygon Poly1;
    initPoly1(Poly1);
    Polygon Poly2;
    initPoly2(Poly2);
    Polygon Poly3;
    initPoly3(Poly3);
    Polygon resultat;
	winInit("MyProg", DIMW, DIMW);
    backgroundColor(0, 0, 0);
	while( !stop )
    {
        winClear();
        resultat = Inter_2poly(Poly1, Poly2, 1/elapsedTime());
        color(255,255,255);
        polygon_draw(resultat);
        stop = winDisplay();
    }
    winQuit();
	return 0;
}

